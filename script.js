//2017 - Pablo Diehl - www.pablodiehl.xyz


//This function send messages to FCM
function sendFireTester() {
    'use strict';
    var sendConfig, sendBody, bodyData;
    try {
        sendConfig = JSON.parse(document.getElementById('send-config').value);
    } catch (confErr) {
        alert('Your Firebase config is not a valid JSON!');
        document.getElementById('send-config').focus();
        return false;
    }
    
    try {
        bodyData = JSON.parse(document.getElementById('send-data').value);
    } catch (bodyErr) {
        alert('Your data is not a valid JSON!');
        document.getElementById('send-data').focus();
        return false;
    }
    
    firebase.initializeApp(sendConfig);
    sendBody = {
		data : bodyData,
		priority: 'high',
		content_available: true,
		to : document.getElementById('send-topic').value
	};
	
    fetch('https://fcm.googleapis.com/fcm/send', {
        method: 'POST',
        headers: new Headers({
            'Content-Type' : 'application/json',
            'Authorization': 'key=' + document.getElementById('send-key').value
        }),
        body: JSON.stringify(sendBody)
    }).then(response => {
        if (response.status < 200 || response.status >= 400) {
            alert('Error while processing the message!\r\nOpen your browser\'s console to see more details.');
            console.error('[Message Processing Error]\r\n', response);
        } else {
            alert('Your message was sent successfully!')
            console.log('[Success]\r\n', response);
        }
    }).catch(error => {
        alert('Failed to connect!\r\nOpen your browser\'s console to see more details.');
        console.error('[Failed to connect]\r\n' + error);
    });
    
    firebase.app().delete();
}

//This function listens to a FCM topic, waiting for messages
function receiveFireTester(){
    var receiveConfig;
    try {
        receiveConfig = JSON.parse(document.getElementById('send-config').value);
    } catch (confErr) {
        alert('Your Firebase config is not a valid JSON!');
        document.getElementById('send-config').focus();
        return false;
    }

    firebase.initializeApp(receiveConfig);
    console.log(firebase.app);
    firebase.app().delete();
}
